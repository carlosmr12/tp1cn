import csv
import os
from sys import argv

GEN = 0

if len(argv) > 1:
    try:
        GEN = int(argv[1])
    except:
        print "Give the number of generations as parameter"

arquivos = [ f for f in os.listdir('.') if f.startswith('statistics') ]

g = 1
best = []
for arquivo in arquivos:
    with open('{}'.format(arquivo), 'rb') as statistics_file:
        reader = csv.reader(statistics_file, delimiter=';')

        for key,row in enumerate(reader):
            g = int(row[0])
            best[key]+= float(row[1])

import ipdb;ipdb.set_trace()
print "Gen {}\nBest {}\nWorse {}".format(g, best/30, worse/30)
