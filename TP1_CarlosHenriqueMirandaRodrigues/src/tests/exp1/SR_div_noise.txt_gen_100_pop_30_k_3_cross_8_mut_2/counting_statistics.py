import csv
import os
from sys import argv

GEN = 0

if len(argv) > 1:
    try:
        GEN = int(argv[1])
    except:
        print "Give the number of generations as parameter"

arquivos = [ f for f in os.listdir('.') if f.startswith('statistics') ]

g = 1
best = 0
worse = 0
average = 0
average_after_crossover = 0
repetitions = 0
gens = []

for arquivo in arquivos:
    with open('{}'.format(arquivo), 'rb') as statistics_file:
        reader = csv.reader(statistics_file, delimiter=';')

        i = 0
        for row in reader:
            g = int(row[0])
            best += float(row[1])
            average += float(row[2])
            average_after_crossover += float(row[3])
            worse += float(row[4])
            repetitions += int(row[5])
            gens[i] = [best/GEN, average/GEN, worse/GEN, repetitions/GEN]
            i += 1

        #print "Gen {}\nBest {}\nAverage {}\nAverage After Crossover {}\nWorse {}\nRepetitions {}".format(g, best/g, worse/g)
import ipdb;ipdb.set_trace()
print "Gen {}\nBest {}\nWorse {}".format(g, best/30, worse/30)
