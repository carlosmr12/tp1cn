# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt

n_groups = 4

means_gen_30 = (7.81466028536, 9.383998494, 9.31058742087, 7.17432869955)

means_gen_50 = (9.96502044513, 8.91482196003, 8.925006399292, 7.46907464076)

means_gen_100 = (9.10585091472, 9.96015492717, 8.261403090538, 7.39930172531)

means_gen_500 = (9.58191970821, 8.988823122917, 8.60450146845, 8.33481674969)

fig, ax = plt.subplots()

index = np.arange(n_groups)
bar_width = 0.15

opacity = 0.7

rects1 = plt.bar(index, means_gen_30, bar_width,
                 alpha=opacity,
                 color='g',
                 label='30')

rects2 = plt.bar(index + bar_width, means_gen_50, bar_width,
                 alpha=opacity,
                 color='b',
                 label='50')

rects3 = plt.bar(index + 2*bar_width, means_gen_100, bar_width,
                 alpha=opacity,
                 color='r',
                 label='100')

rects4 = plt.bar(index + 3*bar_width, means_gen_500, bar_width,
                 alpha=opacity,
                 color='y',
                 label='500')

plt.axis((0, 4, 0, 12))
plt.xlabel(u'População')
plt.ylabel(u'Melhor fitness de cada geração (Erro médio)')
plt.title(u'Experimento 2')
plt.xticks(index + bar_width, ('30', '50', '100', '500'))
plt.legend(bbox_to_anchor=(1,1))

#plt.show()
plt.savefig('exp2.png')
