# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt

n_groups = 4

best = (7.17432869955, 7.46907464076, 7.3993017253, 8.3348167496)

average = (19.23334156, 17.22291131, 17.22028203, 17.74049041)

worse = (367.043818907, 1260.38404895, 213.260777918, 88.3941463759)

fig, ax = plt.subplots()

index = np.arange(n_groups)
bar_width = 0.15

opacity = 0.7

rects1 = plt.bar(index, best, bar_width,
                 alpha=opacity,
                 color='g',
                 label=u'Melhor')

rects2 = plt.bar(index + bar_width, average, bar_width,
                 alpha=opacity,
                 color='b',
                 label=u'Média')

rects3 = plt.bar(index + 2*bar_width, worse, bar_width,
                 alpha=opacity,
                 color='r',
                 label=u'Pior')

#plt.axis((0, 4, 0, 12))
plt.xlabel(u'Gerações')
plt.ylabel(u'Fitness (Erro médio)')
plt.title(u'Experimento 2')
plt.xticks(index + bar_width, ('30', '50', '100', '500'))
plt.legend(bbox_to_anchor=(1,1))

#plt.show()
plt.savefig('exp21.png')
