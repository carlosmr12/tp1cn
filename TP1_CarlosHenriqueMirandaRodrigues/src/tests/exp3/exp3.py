# -*- coding: utf-8 -*-
from matplotlib import pyplot
import csv

y_data_1 = []
y_data_2 = []
y_data_3 = []
y_data_4 = []

with open('gen_30_pop_500_7_3_2.csv', 'rb') as f:
    reader = csv.reader(f, delimiter=";")

    for row in reader:
        y_data_1.append(float(row[1]))
pyplot.plot(y_data_1,label='Crossover {} x Mutation {}'.format(0.7,0.3))
with open('gen_30_pop_500_8_2_2.csv', 'rb') as f:
    reader = csv.reader(f, delimiter=";")

    for row in reader:
        y_data_2.append(float(row[1]))
pyplot.plot(y_data_2,label='Crossover {} x Mutation {}'.format(0.8,0.2))
with open('gen_30_pop_500_9_1_2.csv', 'rb') as f:
    reader = csv.reader(f, delimiter=";")

    for row in reader:
        y_data_3.append(float(row[1]))
pyplot.plot(y_data_3, label = 'Crossover {} x Mutation {}'.format(0.9,0.1))

with open('gen_30_pop_500_10_0_2.csv', 'rb') as f:
    reader = csv.reader(f, delimiter=";")

    for row in reader:
        y_data_4.append(float(row[1]))
pyplot.plot(y_data_4, label = 'Crossover {} x Mutation {}'.format(10,0))

pyplot.title(u"Experimento 3")
pyplot.xlabel(u"Gerações")
pyplot.ylabel(u"Melhor fitness")
#pyplot.legend(bbox_to_anchor=(1,1))
pyplot.savefig('exp3.png')
