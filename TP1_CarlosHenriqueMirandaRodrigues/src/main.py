# -*- coding: utf-8 -*-

"""
TP1 – Regressão Simbólica utilizando Programação Genética (GP)
==============================================================

Carlos Henrique Miranda Rodrigues
chmrodrigues@ufmg.br

DCC831 – Tópicos Especiais em Ciência da Computação - Computação Natural
Departamento de Ciência da Computação
Universidade Federal de Minas Gerais - UFMG
"""
import copy
import csv
import os
import random
from time import time

import config
from treeLib import Tree, Node
from generation import Generation

def main():

    data_graph = {}
    data_average_fitness = {}
    data_best_worse_fitness = {}

    variables_in_file = ''
    with open('fixtures/{}'.format(config.DBFILE), 'r') as f:
        for line in f:
            formatted_line = line.strip()
            config.FILE_LINES.append(formatted_line)
            variables_in_file = formatted_line.split()

    for i in range(0, len(variables_in_file)-1):
        config.NONFUNCTIONS.append(config.VARIABLES[i])

    config.FUNCTIONS_NONFUNCTIONS = config.NONFUNCTIONS + config.FUNCTIONS
    config.CURRENT_VARIABLES = list(set.intersection(set(config.NONFUNCTIONS), set(config.VARIABLES)))

    gen = Generation()
    gen.initialPopulation()

    if not os.path.exists('{}_gen_{}_pop_{}_k_{}_cross_{}_mut_{}'.format(config.DBFILE, config.GENERATIONS, config.POPULATION, config.K, int(config.CROSSOVER_RATE*10), int(config.MUTATION_RATE*10))):
        os.makedirs('{}_gen_{}_pop_{}_k_{}_cross_{}_mut_{}'.format(config.DBFILE, config.GENERATIONS, config.POPULATION, config.K, int(config.CROSSOVER_RATE*10), int(config.MUTATION_RATE*10)))

    with open('{}_gen_{}_pop_{}_k_{}_cross_{}_mut_{}/initial_population_{}.csv'.format(config.DBFILE, config.GENERATIONS, config.POPULATION, config.K, int(config.CROSSOVER_RATE*10), int(config.MUTATION_RATE*10), config.FILE_ID), 'wb') as initial_population_file:
        writer = csv.writer(initial_population_file, delimiter=';')
        for individual in gen.population:
            nodes = []
            individual.getItensInOrder(individual.root, nodes)
            writer.writerow(nodes)


    with open('{}_gen_{}_pop_{}_k_{}_cross_{}_mut_{}/statistics_{}.csv'.format(config.DBFILE, config.GENERATIONS, config.POPULATION, config.K, int(config.CROSSOVER_RATE*10), int(config.MUTATION_RATE*10), config.FILE_ID), 'ab+') as statistics_file:
        generations = []
        for g in range(0, config.GENERATIONS):
            generations.append(gen)
            selected_population = []

            gen.setFitnessPopulation()

            elitism = gen.getBestIndividual()

            gen.setGenerationStatistics()

            data_graph[g] = elitism.getFitnessValueOverDataSet()

            selected_population.append(elitism)

            for i in range(0, config.POPULATION-1):
                selected_population.append(gen.getTournamentWinner(elitism))

            number_of_crossovers = int(config.POPULATION*config.CROSSOVER_RATE)
            number_of_mutations = int(config.POPULATION*config.MUTATION_RATE)

            crossover_counter = 0

            new_gen = Generation()
            new_gen.population.append(elitism)

            new_gen.population = gen.makeCrossover(number_of_crossovers, selected_population, new_gen.population)

            new_gen.setFitnessPopulation()

            average_fitness_after_crossovers = new_gen.getAverageFitness()

            print '{}\t{}\t{}\t{}\t{}'.format(g+1, gen.best_fitness, gen.worse_fitness, gen.average_fitness, gen.repetitions)

            writer = csv.writer(statistics_file, delimiter=';')
            writer.writerow([g+1, gen.best_fitness, gen.average_fitness, average_fitness_after_crossovers, gen.worse_fitness, gen.repetitions]) 

            mutation_group = []

            for i in range(number_of_crossovers, config.POPULATION-1):
                new_gen.mutate(selected_population, elitism)

            new_gen.setFitnessPopulation()

            solution = new_gen.checkSolution()

            if solution:
                print "Solution"
                print solution.fitness_function
                break;

            gen = copy.deepcopy(new_gen)

if __name__ == "__main__":
    from sys import argv

    config.FILE_ID = str(time()).split('.')[0]

    if len(argv) > 1:
        try:
            config.GENERATIONS = int(argv[1])
            config.POPULATION = int(argv[2])
            config.CROSSOVER_RATE = float(argv[3])
            config.MUTATION_RATE = float(argv[4])
            config.K = int(argv[5])
        except:
            print "Something wrong happened. Check the parameters"
            print "A few examples on how to run the script properly (check README.md for more details)"
            print "python main.py"
            print "python main.py 50 100 0.7 0.3 3"
            print "In the last example the parameters are number of generations, number of individuals in the population, crossover rate, mutation rate and the K value for the tournament, respectively"

    main()
