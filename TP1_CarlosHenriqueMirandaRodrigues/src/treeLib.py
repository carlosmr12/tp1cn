# -*- coding: utf-8 -*-
import config
import random

class Node:
    """
    Class Node
    """
    def __init__(self, value):
        self.left = None
        self.data = value
        self.right = None
        self.order = None

class Tree:
    def __init__(self):
        self.node_counter = 0
        self.terminal_nodes = 0
        self.nonterminal_nodes = 0
        self.variables = 0
        self.root = None
        self.fitness_function = ''

    """
    Class tree will provide a tree as well as utility functions.
    """
    def createNode(self, data, insert_ord=0):
        """
        Utility function to create a node.
        """
        new_node = Node(data)
        new_node.order = insert_ord

        if self.node_counter == 0:
            self.root = new_node
        self.node_counter += 1
        if data in config.NONFUNCTIONS:
            self.terminal_nodes += 1
            if data in config.CURRENT_VARIABLES:
                self.variables += 1
        elif data in config.FUNCTIONS:
            self.nonterminal_nodes += 1
        return new_node

    def height(self, node):
        if node is None:
            return 0
        else:
            return max(self.height(node.left), self.height(node.right)) + 1

    def insertFull(self, node , data, insert_ord):
        """
        Insert function will insert a node into tree using the FULL method
        """
        if node is None:
            return self.createNode(data, insert_ord)

        if insert_ord < node.order:
            node.left = self.insertFull(node.left, data, insert_ord)
        else:
            node.right = self.insertFull(node.right, data, insert_ord)

        return node

    def insertGrow(self, node , data, insert_ord):
        """
        Insert function will insert a node into tree using the GROW method
        """
        if node is None:
            return self.createNode(data, insert_ord)
        if node.data in config.NONFUNCTIONS:
            data = ''
        if insert_ord < node.order:
            node.left = self.insertGrow(node.left, data, insert_ord)
        else:
            node.right = self.insertGrow(node.right, data, insert_ord)
        return node

    def calculateFitness(self, root):
        if root is not None:
            self.calculateFitness(root.left)
            self.fitness_function += root.data
            self.calculateFitness(root.right)

    def getFitnessValue(self, values):
        fit_function = self.fitness_function
        try:
            for i in range(0,len(values)):
                fit_function = fit_function.replace(config.CURRENT_VARIABLES[i], str(values[i]))
            return eval(fit_function)
        except ZeroDivisionError:
            return 1000

    def calculateNumberOfNodes(self, node):
        if node is not None:
            self.node_counter += 1
            if node.data in config.NONFUNCTIONS:
                self.terminal_nodes += 1
                if node.data in config.CURRENT_VARIABLES:
                    self.variables += 1
            else:
                self.nonterminal_nodes += 1
            self.calculateNumberOfNodes(node.left)
            self.calculateNumberOfNodes(node.right)

    def setOrdering(self):
        self.initializeNodesOrdering(self.root)

        for j in range(0, len(config.INSERT_ORDER)):
            self.calcNodeOrdering(self.root, config.INSERT_ORDER[j])

        self.node_counter = 0
        self.variables = 0
        self.terminal_nodes = 0
        self.nonterminal_nodes = 0
        self.calculateNumberOfNodes(self.root)

    def makeMutation(self):
        self.setOrdering()

        if random.randint(0,1):
            self.mutation(self.root.left)
        else:
            self.mutation(self.root.right)
        return True

    def mutation(self, node, insert_ord=0):
        if node.left is not None:
            if node.left.data != "":
                if random.randint(0,1):
                    if node.data in config.NONFUNCTIONS:
                        if self.variables == 0:
                            node.data = random.choice(config.CURRENT_VARIABLES)
                        else:
                            node.data = random.choice([x for x in config.NONFUNCTIONS if x!=node.data])
                    else:
                        data_to_insert = random.choice([x for x in config.FUNCTIONS_NONFUNCTIONS if x!=node.data])
                        node.data = data_to_insert
                        if data_to_insert in config.NONFUNCTIONS:
                            node.left = None
                            node.right = None
                else:
                    self.mutation(node.left)
        elif node.right is not None:
            if node.right.data != "":
                if random.randint(0,1):
                    if node.data in config.NONFUNCTIONS:
                        if self.variables == 0:
                            node.data = random.choice(config.CURRENT_VARIABLES)
                        else:
                            node.data = random.choice([x for x in config.NONFUNCTIONS if x!=node.data])
                    else:
                        if self.variables == 0:
                            data_to_insert = random.choice(config.CURRENT_VARIABLES)
                        else:
                            data_to_insert = random.choice([x for x in config.FUNCTIONS_NONFUNCTIONS if x!=node.data])

                        node.data = data_to_insert
                        if data_to_insert in config.NONFUNCTIONS:
                            node.left = None
                            node.right = None
                else:
                    self.mutate(node.right)

        elif node.left is None and node.right is None:
            if self.variables == 0:
                node.data = random.choice(config.CURRENT_VARIABLES)
            else:
                node.data = random.choice([x for x in config.NONFUNCTIONS if x!=node.data])


    def initializeNodesOrdering(self, node):
        if node is not None:
            node.order = 0
            self.initializeNodesOrdering(node.left)
            self.initializeNodesOrdering(node.right)

    def calcNodeOrdering(self, node, insert_ord):
        if node is not None:
            if node.order == 0:
                node.order = insert_ord
                return node

            elif insert_ord < node.order:
                node.left = self.calcNodeOrdering(node.left, insert_ord)
            else:
                node.right = self.calcNodeOrdering(node.right, insert_ord)
        return node

    def getFitnessValueOverDataSet(self):
        f_x = 0

        for line in config.FILE_LINES:
            vet_variables = line.split() 
            variables_values = []
            for i in range(0, len(vet_variables)-1):
                variables_values.append(float(vet_variables[i]))
            y_value = float(vet_variables[-1])

            f_x += (abs(self.getFitnessValue(variables_values) - y_value))

        return f_x

    def getItensInOrder(self, root, nodes_list):
        if root is not None:
            self.getItensInOrder(root.left, nodes_list) 
            nodes_list.append(root.data)
            self.getItensInOrder(root.right, nodes_list)

    def traverseInorder(self, root):
        """
        traverse function will print all the node in the tree.
        """
        if root is not None:
            self.traverseInorder(root.left)
            print root.data
            self.traverseInorder(root.right)

    def traversePreorder(self, root):
        """
        traverse function will print all the node in the tree.
        """
        if root is not None:
            print root.data
            self.traversePreorder(root.left)
            self.traversePreorder(root.right)

    def traversePostorder(self, root):
        """
        traverse function will print all the node in the tree.
        """
        if root is not None:
            self.traversePreorder(root.left)
            self.traversePreorder(root.right)
            print root.data
