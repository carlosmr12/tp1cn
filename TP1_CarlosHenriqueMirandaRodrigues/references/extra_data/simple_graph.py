# -*- coding: utf-8 -*-
from matplotlib import pyplot
import csv

y_data_1 = []
y_data_2 = []
y_data_3 = []
y_data_4 = []

with open('file.csv', 'rb') as f:
    reader = csv.reader(f, delimiter=";")

    for row in reader:
        y_data_1.append(float(row[1]))
pyplot.plot(y_data_1)
with open('file2.csv', 'rb') as f:
    reader = csv.reader(f, delimiter=";")

    for row in reader:
        y_data_2.append(float(row[1]))
pyplot.plot(y_data_2)
with open('file3.csv', 'rb') as f:
    reader = csv.reader(f, delimiter=";")

    for row in reader:
        y_data_3.append(float(row[1]))
pyplot.plot(y_data_3)
with open('file4.csv', 'rb') as f:
    reader = csv.reader(f, delimiter=";")

    for row in reader:
        y_data_4.append(float(row[1]))
pyplot.plot(y_data_4)

pyplot.title(u"Experimento 1")
pyplot.xlabel(u"Gerações")
pyplot.ylabel(u"Melhor fitness")

pyplot.savefig('experimento1.png')
