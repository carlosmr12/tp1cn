# -*- coding: utf-8 -*-
import config
import copy
import random

from treeLib import Tree, Node

class Generation:

    def __init__(self):
        self.population = []
        self.best_fitness = 0
        self.worse_fitness = 0
        self.average_fitness = 0
        self.repetitions = 0

    def initialPopulation(self):
        #   GROW
        for i in range(0, config.POPULATION/2):
            root = None
            ind = Tree()
            root = ind.insertGrow(root, random.choice(config.FUNCTIONS), config.INSERT_ORDER[0])
            for j in range(1, len(config.INSERT_ORDER)):
                if ind.height(ind.root) < 5:
                    node_result = ind.insertGrow(root, random.choice(config.FUNCTIONS_NONFUNCTIONS), config.INSERT_ORDER[j])
                else:
                    if ind.variables == 0:
                        ind.insertGrow(root, random.choice(config.CURRENT_VARIABLES), config.INSERT_ORDER[j])
                    else:
                        ind.insertGrow(root, random.choice(config.NONFUNCTIONS), config.INSERT_ORDER[j])

                if ind.terminal_nodes - ind.nonterminal_nodes == 1:
                    break

            self.population.append(ind)

        #   FULL
        for i in range(config.POPULATION/2, config.POPULATION):
            root = None
            ind = Tree()
            root = ind.insertFull(root, random.choice(config.FUNCTIONS), config.INSERT_ORDER[0])

            for j in range(1, len(config.INSERT_ORDER)):

                data_to_insert = random.choice(config.FUNCTIONS)
                if j > len(config.INSERT_ORDER)/2 -1:
                    if ind.variables == 0:
                        data_to_insert = random.choice(config.CURRENT_VARIABLES)
                    else:
                        data_to_insert = random.choice(config.NONFUNCTIONS)

                ind.insertFull(root, data_to_insert, config.INSERT_ORDER[j])

            ind.calculateFitness(ind.root)

            self.population.append(ind)

        return True

    def setFitnessPopulation(self):
        for ind in self.population:
            ind.fitness_function = ''
            ind.calculateFitness(ind.root)

    def getBestIndividual(self, population=None):
        if population == None:
            population = self.population
        best_individual = None
        best_individual_f_x = config.MAXINT
        for i in range(0, len(population)):
            f_x_ind = 0
    
            try:
                eval(population[i].fitness_function)
            except:
                for line in config.FILE_LINES:
                    vet_variables = line.strip().split() 
                    variables_values = []
                    for j in range(0, len(vet_variables)-1):
                        variables_values.append(float(vet_variables[j]))
                    y_value = float(vet_variables[-1])
    
                    f_x_ind += (abs(population[i].getFitnessValue(variables_values) - y_value))
    
                if f_x_ind < best_individual_f_x:
                    best_individual = population[i]
                    best_individual_f_x = f_x_ind
    
        return best_individual

    #def getBestFitness(self):
        #best_individual_f_x = config.MAXINT
        #for i in range(0, config.POPULATION):
            #f_x_ind = 0
#
            #try:
                #eval(self.population[i].fitness_function)
            #except:
                #for line in config.FILE_LINES:
                    #vet_variables = line.split() 
                    #variables_values = []
                    #for j in range(0, len(vet_variables)-1):
                        #variables_values.append(float(vet_variables[j]))
                    #y_value = float(vet_variables[-1])
#
                    #f_x_ind += (abs(self.population[i].getFitnessValue(variables_values) - y_value))
#
                #if f_x_ind < best_individual_f_x:
                    #best_individual_f_x = f_x_ind
#
        #return best_individual_f_x 

    #def getWorseFitness(self):
        #worse_individual_f_x = 0
        #for i in range(0, config.POPULATION):
            #f_x_ind = 0
#
            #try:
                #eval(self.population[i].fitness_function)
            #except:
                #for line in config.FILE_LINES:
                    #vet_variables = line.split() 
                    #variables_values = []
                    #for j in range(0, len(vet_variables)-1):
                        #variables_values.append(float(vet_variables[j]))
                    #y_value = float(vet_variables[-1])
#
                    #f_x_ind += (abs(self.population[i].getFitnessValue(variables_values) - y_value))
#
                #if f_x_ind > worse_individual_f_x:
                    #worse_individual_f_x = f_x_ind
#
        #return worse_individual_f_x 

    def getAverageFitness(self):
        average_fitness = 0
        for i in range(0, len(self.population)):
            f_x_ind = 0

            try:
                eval(self.population[i].fitness_function)
            except:
                for line in config.FILE_LINES:
                    vet_variables = line.split() 
                    variables_values = []
                    for j in range(0, len(vet_variables)-1):
                        variables_values.append(float(vet_variables[j]))
                    y_value = float(vet_variables[-1])

                    f_x_ind += (abs(self.population[i].getFitnessValue(variables_values) - y_value))

            average_fitness += f_x_ind
        return average_fitness/len(self.population)

    def getRepetitions(self):
        fitness_functions = []
        for item in self.population:
            fitness_functions.append(item.fitness_function)
        return len(fitness_functions) - len(list(set(fitness_functions)))

    def setGenerationStatistics(self):
        best_individual_f_x = config.MAXINT
        worse_individual_f_x = 0
        average_fitness = 0
        for i in range(0, config.POPULATION):
            f_x_ind = 0

            try:
                eval(self.population[i].fitness_function)
            except:
                for line in config.FILE_LINES:
                    vet_variables = line.split() 
                    variables_values = []
                    for j in range(0, len(vet_variables)-1):
                        variables_values.append(float(vet_variables[j]))
                    y_value = float(vet_variables[-1])

                    f_x_ind += (abs(self.population[i].getFitnessValue(variables_values) - y_value))

                if f_x_ind < best_individual_f_x:
                    best_individual_f_x = f_x_ind

                if f_x_ind > worse_individual_f_x:
                    worse_individual_f_x = f_x_ind
            average_fitness += f_x_ind

        self.best_fitness = best_individual_f_x
        self.worse_fitness = worse_individual_f_x
        self.average_fitness = average_fitness/len(self.population)
        self.repetitions = self.getRepetitions()

        return True

    def getTournamentWinner(self, elitism):
        tournament = []
        for i in range(0, config.K):
            tournament.append(random.choice([x for x in self.population if x!=elitism]))

        return self.getBestIndividual(tournament)

    def makeCrossover(self, number_of_crossovers, selected_population, new_population):
        crossover_counter = 0
        while 1:
            father = random.choice(selected_population)
            while father is None:
                father = random.choice(selected_population)

            mother = random.choice([x for x in selected_population if x!=father and x!=selected_population])
            while mother is None:
                mother = random.choice([x for x in selected_population if x!=father and x!=selected_population])

            son = copy.deepcopy(father)

            crossover_counter += 1
            new_population.append(son)

            if crossover_counter == number_of_crossovers:
                return new_population

            daughter = copy.deepcopy(mother)
            daughter.root.right = copy.deepcopy(father.root.left)
            crossover_counter += 1

            new_population.append(daughter)

            if crossover_counter == number_of_crossovers:
                return new_population

    def initializePopulationCounters(self):
        for ind in self.population:
            ind.node_counter = 0
            ind.variables = 0
            ind.terminal_nodes = 0
            ind.nonterminal_nodes = 0
            ind.calculateNumberOfNodes(ind.root)

    def mutate(self, selected_population, elitism):
        individual_to_mutate = None
        while individual_to_mutate == None:
            individual_to_mutate = random.choice([x for x in selected_population if x!=elitism])
        individual_to_mutate.makeMutation()
        self.population.append(individual_to_mutate)
        return True

    def checkSolution(self):
        best_individual = None
        best_individual_f_x = config.MAXINT
        for i in range(0, config.POPULATION):
            f_x_ind = 0

            try:
                eval(self.population[i].fitness_function)
            except:
                for line in config.FILE_LINES:
                    vet_variables = line.split() 
                    variables_values = []
                    for j in range(0, len(vet_variables)-1):
                        variables_values.append(float(vet_variables[j]))
                    y_value = float(vet_variables[-1])

                    f_x_ind += (abs(self.population[i].getFitnessValue(variables_values) - y_value))

                if f_x_ind < best_individual_f_x:
                    best_individual = self.population[i]
                    best_individual_f_x = f_x_ind

        if best_individual_f_x == 0:
            return best_individual

        return None
