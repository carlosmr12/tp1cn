
#def plot_graph_individual(individual):
    #from matplotlib import pyplot
#
    #x_data = []
    #y_data = []
#
    #if config.Z in config.NONFUNCTIONS:
        #t_data = []
        #u_data = []
        #v_data = []
        #w_data = []
        #z_data = []
#
        #for line in config.FILE_LINES:
            #t_data.append(float(line.split(' ')[0]))
            #u_data.append(float(line.split(' ')[1]))
            #v_data.append(float(line.split(' ')[2]))
            #x_data.append(float(line.split(' ')[3]))
            #w_data.append(float(line.split(' ')[4]))
            #z_data.append(float(line.split(' ')[5]))
#
    #else:
        #for line in config.FILE_LINES:
            #x_data.append(float(line.split(' ')[0]))
#
        #for x in x_data:
            #y = individual.getFitnessValue(x)
            #pyplot.scatter(x, y)
#
        #pyplot.savefig('{}_gen_{}_pop_{}_k_{}_cross_{}_mut_{}/gen_{}_pop_{}_k_{}_cross_{}_mut_{}_{}'.format(config.DBFILE, config.GENERATIONS, config.POPULATION, config.K, int(config.CROSSOVER_RATE*10), int(config.MUTATION_RATE*10), config.GENERATIONS, config.POPULATION, config.K, int(config.CROSSOVER_RATE*10), int(config.MUTATION_RATE*10), str(time()).split('.')[0]))
#

#def initialPopulation(population=config.POPULATION, individual_size=config.INDIVIDUAL_SIZE):
    #individuals = []
    ##   GROW
    #for i in range(0, population/2):
        #root = None
        #ind = Tree()
        #root = ind.insertGrow(root, random.choice(config.FUNCTIONS), config.INSERT_ORDER[0])
        #for j in range(1, len(config.INSERT_ORDER)):
            #if ind.height(ind.root) < 5:
                #node_result = ind.insertGrow(root, random.choice(config.FUNCTIONS_NONFUNCTIONS), config.INSERT_ORDER[j])
            #else:
                #if ind.variables == 0:
                    #ind.insertGrow(root, random.choice(config.CURRENT_VARIABLES), config.INSERT_ORDER[j])
                #else:
                    #ind.insertGrow(root, random.choice(config.NONFUNCTIONS), config.INSERT_ORDER[j])
#
            #if ind.terminal_nodes - ind.nonterminal_nodes == 1:
                #break
#
        #individuals.append(ind)
#
    ##   FULL
    #for i in range(population/2, population):
        #root = None
        #ind = Tree()
        #root = ind.insertFull(root, random.choice(config.FUNCTIONS), config.INSERT_ORDER[0])
#
        #for j in range(1, len(config.INSERT_ORDER)):
#
            #data_to_insert = random.choice(config.FUNCTIONS)
            #if j > len(config.INSERT_ORDER)/2 -1:
                #if ind.variables == 0:
                    #data_to_insert = random.choice(config.CURRENT_VARIABLES)
                #else:
                    #data_to_insert = random.choice(config.NONFUNCTIONS)
#
            #ind.insertFull(root, data_to_insert, config.INSERT_ORDER[j])
#
        #ind.calculateFitness(ind.root)
#
        #individuals.append(ind)
#
    #return individuals
#
#def getBestIndividual(population):
    #best_individual = None
    #best_individual_f_x = config.MAXINT
    #for i in range(0, len(population)):
        #f_x_ind = 0
#
        #try:
            #eval(population[i].fitness_function)
        #except:
            #for line in config.FILE_LINES:
                #vet_variables = line.strip().split() 
                #variables_values = []
                #for j in range(0, len(vet_variables)-1):
                    #try:
                        #variables_values.append(float(vet_variables[j]))
                    #except:
                        #import ipdb;ipdb.set_trace()
                #y_value = float(vet_variables[-1])
#
                #f_x_ind += (abs(population[i].getFitnessValue(variables_values) - y_value))
#
            #if f_x_ind < best_individual_f_x:
                #best_individual = population[i]
                #best_individual_f_x = f_x_ind
#
    #return best_individual
#
#def getBestFitness(population):
    #best_individual_f_x = config.MAXINT
    #for i in range(0, len(population)):
        #f_x_ind = 0
#
        #try:
            #eval(population[i].fitness_function)
        #except:
            #for line in config.FILE_LINES:
                #vet_variables = line.split() 
                #variables_values = []
                #for j in range(0, len(vet_variables)-1):
                    #variables_values.append(float(vet_variables[j]))
                #y_value = float(vet_variables[-1])
#
                #f_x_ind += (abs(population[i].getFitnessValue(variables_values) - y_value))
#
            #if f_x_ind < best_individual_f_x:
                #best_individual_f_x = f_x_ind
#
    #return best_individual_f_x 
#
#def getWorseFitness(population):
    #worse_individual_f_x = 0
    #for i in range(0, len(population)):
        #f_x_ind = 0
#
        #try:
            #eval(population[i].fitness_function)
        #except:
            #for line in config.FILE_LINES:
                #vet_variables = line.split() 
                #variables_values = []
                #for j in range(0, len(vet_variables)-1):
                    #variables_values.append(float(vet_variables[j]))
                #y_value = float(vet_variables[-1])
#
                #f_x_ind += (abs(population[i].getFitnessValue(variables_values) - y_value))
#
            #if f_x_ind > worse_individual_f_x:
                #worse_individual_f_x = f_x_ind
#
    #return worse_individual_f_x 
#
#def getAverageFitness(population):
    #average_fitness = 0
    #for i in range(0, len(population)):
        #f_x_ind = 0
#
        #try:
            #eval(population[i].fitness_function)
        #except:
            #for line in config.FILE_LINES:
                #vet_variables = line.split() 
                #variables_values = []
                #for j in range(0, len(vet_variables)-1):
                    #variables_values.append(float(vet_variables[j]))
                #y_value = float(vet_variables[-1])
#
                #f_x_ind += (abs(population[i].getFitnessValue(variables_values) - y_value))
#
        #average_fitness += f_x_ind
    #return average_fitness/len(population)
#
#def makeMutation(individual):
    #setOrdering(individual)
#
    #if random.randint(0,1):
        #individual.mutate(individual.root.left)
    #else:
        #individual.mutate(individual.root.right)
    #return individual

#def checkSolution(population):
    #best_individual = None
    #best_individual_f_x = config.MAXINT
    #for i in range(0, len(population)):
        #f_x_ind = 0
#
        #try:
            #eval(population[i].fitness_function)
        #except:
            #for line in config.FILE_LINES:
                #vet_variables = line.split() 
                #variables_values = []
                #for j in range(0, len(vet_variables)-1):
                    #variables_values.append(float(vet_variables[j]))
                #y_value = float(vet_variables[-1])
#
                #f_x_ind += (abs(population[i].getFitnessValue(variables_values) - y_value))
#
            #if f_x_ind < best_individual_f_x:
                #best_individual = population[i]
                #best_individual_f_x = f_x_ind
#
    #if best_individual_f_x == 0:
        #return best_individual
#
    #return None

#def setOrdering(ind):
    #ind.initializeNodesOrdering(ind.root)
#
    #for j in range(0, len(config.INSERT_ORDER)):
        #ind.calcNodeOrdering(ind.root, config.INSERT_ORDER[j])
#
    #ind.node_counter = 0
    #ind.variables = 0
    #ind.terminal_nodes = 0
    #ind.nonterminal_nodes = 0
    #ind.calculateNumberOfNodes(ind.root)
