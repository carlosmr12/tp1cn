# -*- coding: utf-8 -*-
from matplotlib import pyplot
import csv

y_data_1 = []
y_data_2 = []
y_data_3 = []
y_data_4 = []

label = "yacht_hydrodynamics.txt"

#with open('SR_div_noise_100_100_3_8_2.csv', 'rb') as f:
    #reader = csv.reader(f, delimiter=";")
#
    #for row in reader:
        #y_data_1.append(float(row[1]))
#pyplot.plot(y_data_1, label=label)

#with open('SR_ellipse_noise_positive_100_100_3_8_2.csv', 'rb') as f:
    #reader = csv.reader(f, delimiter=";")
#
    #for row in reader:
        #y_data_1.append(float(row[1]))
#pyplot.plot(y_data_1, label=label)

with open('yatch_hydrodynamics_100_100_3_8_2.csv', 'rb') as f:
    reader = csv.reader(f, delimiter=";")

    for row in reader:
        y_data_3.append(float(row[1]))
pyplot.plot(y_data_3, label=label)

pyplot.title(u"Experimento 1")
pyplot.xlabel(u"Gerações")
pyplot.ylabel(u"Melhor fitness")
pyplot.legend(bbox_to_anchor=(1,1))

pyplot.savefig('exp1_{}.png'.format(label))
