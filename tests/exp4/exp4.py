# -*- coding: utf-8 -*-
from matplotlib import pyplot
import csv

y_data_1 = []
y_data_2 = []

with open('gen_30_pop_500_7_3_2.csv', 'rb') as f:
    reader = csv.reader(f, delimiter=";")

    for row in reader:
        y_data_1.append(float(row[1]))
pyplot.plot(y_data_1,label='K=2')

with open('gen_30_pop_500_7_3_5.csv', 'rb') as f:
    reader = csv.reader(f, delimiter=";")

    for row in reader:
        y_data_2.append(float(row[1]))
pyplot.plot(y_data_2,label='K=5')

pyplot.title(u"Experimento 4")
pyplot.xlabel(u"Gerações")
pyplot.ylabel(u"Melhor fitness")
pyplot.legend(bbox_to_anchor=(1,1))
pyplot.savefig('exp4.png')
