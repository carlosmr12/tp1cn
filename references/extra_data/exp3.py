# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt


n_groups = 3

means_gen_50 = (40, 25, 35)

means_gen_100 = (10, 15, 40)

means_gen_500 = (10, 35, 36)

fig, ax = plt.subplots()

index = np.arange(n_groups)
bar_width = 0.15

opacity = 0.7

rects1 = plt.bar(index, means_gen_50, bar_width,
                 alpha=opacity,
                 color='g',
                 label=u'Melhor')

rects2 = plt.bar(index + bar_width, means_gen_100, bar_width,
                 alpha=opacity,
                 color='b',
                 label=u'Média')

rects3 = plt.bar(index + 2*bar_width, means_gen_500, bar_width,
                 alpha=opacity,
                 color='r',
                 label=u'Pior')

plt.xlabel(u'Geração')
plt.ylabel(u'Valores de Fitness')
plt.title(u'Experimento 3')
plt.xticks(index + bar_width, (u'50', u'100', u'500'))
plt.legend()

#plt.tight_layout()
plt.show()
