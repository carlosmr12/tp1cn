# -*- coding: utf-8 -*-

from math import pow
from random import uniform
from sys import maxint

DBFILE = 'yacht_hydrodynamics.txt'

FILE_ID = ''

MAXINT = maxint

HAS_ELITISM = True

GENERATIONS = 50
POPULATION = 30
INDIVIDUAL_SIZE = int(pow(2,7-1))

A = str(uniform(0, 1))
B = str(uniform(1, 2))

T = 't'
U = 'u'
X = 'x'
V = 'v'
W = 'w'
Z = 'z'

# TODO add POWER, LOG, RADICIAÇÃO
FUNCTIONS = ['+', '-', '/', '*']
CONSTANTS = [A, B]
VARIABLES = [X, T, U, V, W, Z]

NONFUNCTIONS = CONSTANTS

FUNCTIONS_NONFUNCTIONS = None
CURRENT_VARIABLES = None

CROSSOVER_RATE = 0.9
MUTATION_RATE = 0.1

K = 2

FILE_LINES = []

INSERT_ORDER = [32,16,48,8,24,40,56,4,12,20,28,36,44,52,60,2,6,10,14,18,22,26,30,34,38,42,46,50,54,58,62,1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47,49,51,53,55,57,59,61,63]
