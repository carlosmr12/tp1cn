# -*- coding: utf-8 -*-

import copy
import csv
import os
import random
from time import time

import config
from treeLib import Tree, Node

def plot_graph_individual(individual):
    from matplotlib import pyplot

    x_data = []
    y_data = []

    if config.Z in config.NONFUNCTIONS:
        t_data = []
        u_data = []
        v_data = []
        w_data = []
        z_data = []

        for line in config.FILE_LINES:
            t_data.append(float(line.split(' ')[0]))
            u_data.append(float(line.split(' ')[1]))
            v_data.append(float(line.split(' ')[2]))
            x_data.append(float(line.split(' ')[3]))
            w_data.append(float(line.split(' ')[4]))
            z_data.append(float(line.split(' ')[5]))

    else:
        for line in config.FILE_LINES:
            x_data.append(float(line.split(' ')[0]))

        for x in x_data:
            y = individual.getFitnessValue(x)
            pyplot.scatter(x, y)

        pyplot.savefig('{}_gen_{}_pop_{}_k_{}_cross_{}_mut_{}/gen_{}_pop_{}_k_{}_cross_{}_mut_{}_{}'.format(config.DBFILE, config.GENERATIONS, config.POPULATION, config.K, int(config.CROSSOVER_RATE*10), int(config.MUTATION_RATE*10), config.GENERATIONS, config.POPULATION, config.K, int(config.CROSSOVER_RATE*10), int(config.MUTATION_RATE*10), str(time()).split('.')[0]))

def initialPopulation(population=config.POPULATION, individual_size=config.INDIVIDUAL_SIZE):
    individuals = []
    #   GROW
    for i in range(0, population/2):
        root = None
        ind = Tree()
        root = ind.insertGrow(root, random.choice(config.FUNCTIONS), config.INSERT_ORDER[0])
        for j in range(1, len(config.INSERT_ORDER)):
            if ind.height(ind.root) < 5:
                node_result = ind.insertGrow(root, random.choice(config.FUNCTIONS_NONFUNCTIONS), config.INSERT_ORDER[j])
            else:
                if ind.variables == 0:
                    ind.insertGrow(root, random.choice(config.CURRENT_VARIABLES), config.INSERT_ORDER[j])
                else:
                    ind.insertGrow(root, random.choice(config.NONFUNCTIONS), config.INSERT_ORDER[j])

            if ind.terminal_nodes - ind.nonterminal_nodes == 1:
                break

        individuals.append(ind)

    #   FULL
    for i in range(population/2, population):
        root = None
        ind = Tree()
        root = ind.insertFull(root, random.choice(config.FUNCTIONS), config.INSERT_ORDER[0])

        for j in range(1, len(config.INSERT_ORDER)):

            data_to_insert = random.choice(config.FUNCTIONS)
            if j > len(config.INSERT_ORDER)/2 -1:
                if ind.variables == 0:
                    data_to_insert = random.choice(config.CURRENT_VARIABLES)
                else:
                    data_to_insert = random.choice(config.NONFUNCTIONS)

            ind.insertFull(root, data_to_insert, config.INSERT_ORDER[j])

        ind.calculateFitness(ind.root)

        individuals.append(ind)

    return individuals

def getBestIndividual(population):
    best_individual = None
    best_individual_f_x = config.MAXINT
    for i in range(0, len(population)):
        f_x_ind = 0

        try:
            eval(population[i].fitness_function)
        except:
            for line in config.FILE_LINES:
                vet_variables = line.strip().split() 
                variables_values = []
                for j in range(0, len(vet_variables)-1):
                    try:
                        variables_values.append(float(vet_variables[j]))
                    except:
                        import ipdb;ipdb.set_trace()
                y_value = float(vet_variables[-1])

                f_x_ind += (abs(population[i].getFitnessValue(variables_values) - y_value))

            if f_x_ind < best_individual_f_x:
                best_individual = population[i]
                best_individual_f_x = f_x_ind

    return best_individual

def getBestFitness(population):
    best_individual_f_x = config.MAXINT
    for i in range(0, len(population)):
        f_x_ind = 0

        try:
            eval(population[i].fitness_function)
        except:
            for line in config.FILE_LINES:
                vet_variables = line.split() 
                variables_values = []
                for j in range(0, len(vet_variables)-1):
                    variables_values.append(float(vet_variables[j]))
                y_value = float(vet_variables[-1])

                f_x_ind += (abs(population[i].getFitnessValue(variables_values) - y_value))

            if f_x_ind < best_individual_f_x:
                best_individual_f_x = f_x_ind

    return best_individual_f_x 

def getWorseFitness(population):
    worse_individual_f_x = 0
    for i in range(0, len(population)):
        f_x_ind = 0

        try:
            eval(population[i].fitness_function)
        except:
            for line in config.FILE_LINES:
                vet_variables = line.split() 
                variables_values = []
                for j in range(0, len(vet_variables)-1):
                    variables_values.append(float(vet_variables[j]))
                y_value = float(vet_variables[-1])

                f_x_ind += (abs(population[i].getFitnessValue(variables_values) - y_value))

            if f_x_ind > worse_individual_f_x:
                worse_individual_f_x = f_x_ind

    return worse_individual_f_x 

def getAverageFitness(population):
    average_fitness = 0
    for i in range(0, len(population)):
        f_x_ind = 0

        try:
            eval(population[i].fitness_function)
        except:
            for line in config.FILE_LINES:
                vet_variables = line.split() 
                variables_values = []
                for j in range(0, len(vet_variables)-1):
                    variables_values.append(float(vet_variables[j]))
                y_value = float(vet_variables[-1])

                f_x_ind += (abs(population[i].getFitnessValue(variables_values) - y_value))

        average_fitness += f_x_ind
    return average_fitness/len(population)

def makeMutation(individual):
    setOrdering(individual)

    if random.randint(0,1):
        individual.mutate(individual.root.left)
    else:
        individual.mutate(individual.root.right)
    return individual

def checkSolution(population):
    best_individual = None
    best_individual_f_x = config.MAXINT
    for i in range(0, len(population)):
        f_x_ind = 0

        try:
            eval(population[i].fitness_function)
        except:
            for line in config.FILE_LINES:
                vet_variables = line.split() 
                variables_values = []
                for j in range(0, len(vet_variables)-1):
                    variables_values.append(float(vet_variables[j]))
                y_value = float(vet_variables[-1])

                f_x_ind += (abs(population[i].getFitnessValue(variables_values) - y_value))

            if f_x_ind < best_individual_f_x:
                best_individual = population[i]
                best_individual_f_x = f_x_ind

    if best_individual_f_x == 0:
        return best_individual

    return None

def setOrdering(ind):
    ind.initializeNodesOrdering(ind.root)

    for j in range(0, len(config.INSERT_ORDER)):
        ind.calcNodeOrdering(ind.root, config.INSERT_ORDER[j])

    ind.node_counter = 0
    ind.variables = 0
    ind.terminal_nodes = 0
    ind.nonterminal_nodes = 0
    ind.calculateNumberOfNodes(ind.root)

def main():

    data_graph = {}
    data_average_fitness = {}
    data_best_worse_fitness = {}

    variables_in_file = ''
    with open('fixtures/{}'.format(config.DBFILE), 'r') as f:
        for line in f:
            formatted_line = line.strip()
            config.FILE_LINES.append(formatted_line)
            variables_in_file = formatted_line.split()

    for i in range(0, len(variables_in_file)-1):
        config.NONFUNCTIONS.append(config.VARIABLES[i])

    config.FUNCTIONS_NONFUNCTIONS = config.NONFUNCTIONS + config.FUNCTIONS
    config.CURRENT_VARIABLES = list(set.intersection(set(config.NONFUNCTIONS), set(config.VARIABLES)))

    population = initial_population = initialPopulation(config.POPULATION)

    if not os.path.exists('{}_gen_{}_pop_{}_k_{}_cross_{}_mut_{}'.format(config.DBFILE, config.GENERATIONS, config.POPULATION, config.K, int(config.CROSSOVER_RATE*10), int(config.MUTATION_RATE*10))):
        os.makedirs('{}_gen_{}_pop_{}_k_{}_cross_{}_mut_{}'.format(config.DBFILE, config.GENERATIONS, config.POPULATION, config.K, int(config.CROSSOVER_RATE*10), int(config.MUTATION_RATE*10)))

    with open('{}_gen_{}_pop_{}_k_{}_cross_{}_mut_{}/initial_population_{}.csv'.format(config.DBFILE, config.GENERATIONS, config.POPULATION, config.K, int(config.CROSSOVER_RATE*10), int(config.MUTATION_RATE*10), config.FILE_ID), 'wb') as initial_population_file:
        writer = csv.writer(initial_population_file, delimiter=';')
        for individual in initial_population:
            nodes = []
            individual.getItensInOrder(individual.root, nodes)
            writer.writerow(nodes)

    pop_generations = []
    for g in range(0, config.GENERATIONS):
        pop_generations.append(population)
        selected_population = []

        for ind in population:
            ind.fitness_function = ''
            ind.calculateFitness(ind.root)

        elitism = getBestIndividual(population)

        best_fitness = getBestFitness(population)
        worse_fitness = getWorseFitness(population)
        average_fitness = getAverageFitness(population)
        print "Generation {} - {}\t{}\t{}".format(g, best_fitness, worse_fitness, average_fitness)

        with open('{}_gen_{}_pop_{}_k_{}_cross_{}_mut_{}/best_worse_fitness_{}.csv'.format(config.DBFILE, config.GENERATIONS, config.POPULATION, config.K, int(config.CROSSOVER_RATE*10), int(config.MUTATION_RATE*10), config.FILE_ID), 'ab+') as best_worse_file:
            writer = csv.writer(best_worse_file, delimiter=';')
            writer.writerow([g+1, best_fitness, average_fitness, worse_fitness]) 

        data_graph[g] = elitism.getFitnessValueOverDataSet()

        selected_population.append(elitism)

        for i in range(0, config.POPULATION-1):
            tournament = []
            for i in range(0, config.K):
                tournament.append(random.choice([x for x in population if x!=elitism]))

            selected_population.append(getBestIndividual(tournament))

        number_of_crossovers = int(len(selected_population)*config.CROSSOVER_RATE)
        number_of_mutations = int(len(selected_population)*config.MUTATION_RATE)

        crossover_group = []

        crossover_counter = 0

        new_population = []
        new_population.append(elitism)

        while 1:
            father = random.choice(selected_population)
            #if selected_population is None:
                #import ipdb;ipdb.set_trace()
            while father is None:
                father = random.choice(selected_population)

            mother = random.choice([x for x in selected_population if x!=father and x!=selected_population])
            while mother is None:
                mother = random.choice([x for x in selected_population if x!=father and x!=selected_population])

            son = copy.deepcopy(father)

            try:
                son.root.left = copy.deepcopy(mother.root.right)
            except:
                import ipdb;ipdb.set_trace()

            crossover_counter += 1
            crossover_group.append(son)
            new_population.append(son)

            if crossover_counter == number_of_crossovers:
                break;

            daughter = copy.deepcopy(mother)
            daughter.root.right = copy.deepcopy(father.root.left)
            crossover_counter += 1

            crossover_group.append(daughter)
            new_population.append(daughter)

            if crossover_counter == number_of_crossovers:
                break;

        for ind in new_population:
            ind.node_counter = 0
            ind.variables = 0
            ind.terminal_nodes = 0
            ind.nonterminal_nodes = 0
            ind.calculateNumberOfNodes(ind.root)

        average_fitness_after_crossovers = getAverageFitness(new_population)

        with open('{}_gen_{}_pop_{}_k_{}_cross_{}_mut_{}/average_fitness_after_crossovers_{}.csv'.format(config.DBFILE, config.GENERATIONS, config.POPULATION, config.K, int(config.CROSSOVER_RATE*10), int(config.MUTATION_RATE*10), config.FILE_ID), 'ab+') as f:
            writer = csv.writer(f, delimiter=';')
            writer.writerow([g+1, average_fitness, average_fitness_after_crossovers]) 

        mutation_group = []

        for i in range(number_of_crossovers, len(selected_population)):
            individual_to_mutate = None
            while individual_to_mutate == None:
                individual_to_mutate = random.choice([x for x in selected_population if x!=elitism])
            ind = makeMutation(individual_to_mutate)

        for ind in new_population:
            ind.fitness_function = ''
            ind.calculateFitness(ind.root)

        solution = checkSolution(new_population)

        if solution:
            print "Solution"
            print solution.fitness_function
            break;

        population = copy.deepcopy(new_population)

    with open('{}_gen_{}_pop_{}_k_{}_cross_{}_mut_{}/generationXbestsolution_{}.csv'.format(config.DBFILE, config.GENERATIONS, config.POPULATION, config.K, int(config.CROSSOVER_RATE*10), int(config.MUTATION_RATE*10), config.FILE_ID), 'wb') as csvfile:
        writer = csv.writer(csvfile, delimiter=';')

        for key,value in data_graph.iteritems():
            writer.writerow([key, value])

    best_solution = getBestIndividual(population)

    best_individual_f_x = 0
    for line in config.FILE_LINES:
        vet_variables = line.split() 
        variables_values = []
        for j in range(0, len(vet_variables)-1):
            variables_values.append(float(vet_variables[j]))
        y = float(vet_variables[-1])

        best_individual_f_x += (abs(best_solution.getFitnessValue(variables_values) - y))

    #plot_graph_individual(best_solution)


if __name__ == "__main__":
    from sys import argv

    config.FILE_ID = str(time()).split('.')[0]

    if len(argv) > 1:
        try:
            config.GENERATIONS = int(argv[1])
            config.POPULATION = int(argv[2])
            config.CROSSOVER_RATE = float(argv[3])
            config.MUTATION_RATE = float(argv[4])
            config.K = int(argv[5])
        except:
            print "Something wrong happened. Check the parameters"
            print "A few examples on how to run the script properly (check README.md for more details)"
            print "python main.py"
            print "python main.py 50 100 0.7 0.3 3"
            print "In the last example the parameters are number of generations, number of individuals in the population, crossover rate, mutation rate and the K value for the tournament, respectively"

    main()
