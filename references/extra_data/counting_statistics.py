import csv
import os
from sys import argv

GEN = 0

if len(argv) > 1:
    try:
        GEN = int(argv[1])
    except:
        print "Give the number of generations as parameter"

arquivos = [ f for f in os.listdir('.') if f.startswith('statistics') ]

g = 1
best = 0
worse = 0
average = 0
average_after_crossover = 0
repetitions = 0
for arquivo in arquivos:
    with open('{}'.format(arquivo), 'rb') as statistics_file:
        reader = csv.reader(statistics_file, delimiter=';')

        for row in reader:
            g = int(row[0])
            if g == GEN:
                best += float(row[1])
                average += float(row[2])
                average_after_crossover += float(row[3])
                worse += float(row[4])
                repetitions += int(row[5])

        #print "Gen {}\nBest {}\nAverage {}\nAverage After Crossover {}\nWorse {}\nRepetitions {}".format(g, best/g, worse/g)
print "Gen {}\nBest {}\nWorse {}".format(g, best/30, worse/30)
